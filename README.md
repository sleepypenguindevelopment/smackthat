# Smack That #

This game was built during the Meatly Game Jam in March, 2015 (http://www.meatlyjam.com/).  "Old Idea" zombies approach the player, who uses the Leap Motion controller to "smack" the zombies away.  

Gameplay video at https://www.youtube.com/watch?v=tUvKPTQOifk

**This repository may not be the complete project:** There may be assets imported from the Unity Asset Store that are not included in this repository.