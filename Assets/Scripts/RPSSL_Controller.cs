﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Leap;
//using TMPro;

public class RPSSL_Controller : MonoBehaviour {

	/*
	public Object rockModel;
	public Object paperModel;
	public Object scissorsModel;
	public Object spockModel;
	public Object lizardModel;

	public Text player1ScoreText;
	public Text player2ScoreText;
	public Text player1MissText;
	public Text player2MissText;
	public Text player1CountText;
	public Text player2CountText;
	public Text player1ResultText;
	public Text player2ResultText;
	public Text player1KeyboardText;
	public Text player2KeyboardText;
	public Text winnerText;
	public Text verbText;
	public Text loserText;
	public Text instructionsText; 
	public RectTransform splashScreenPanel;

	public AudioClip countSound;
	public AudioClip shootSound;
	public AudioClip winSound;
	public AudioClip tieSound;

	public float thumbIndexDistance;
	public float middleRingDistance;

	Controller controller;
	Frame frame;

	Vector4 clear;
	Vector4 rock;
	Vector4 scissors;
	Vector4 all;

	bool twoPlayers;
	bool player1Ready;
	bool player2Ready;
	bool counting;
	bool keyboardMode;
	bool splashAnimating;

	int count;
	float countDown;
	string player1Result;
	string player2Result;
	int player1Score;
	int player2Score;
	Object player1Model;
	Object player2Model;
	Text startText;

	AudioSource sounds;

	Dictionary<string, string> verbs = new Dictionary<string, string>();

	void Start ()
	{
		// initialize the Leap Motion controller
		controller = new Controller();
		frame = null;
		// enable the circle gesture
		controller.EnableGesture(Gesture.GestureType.TYPE_SWIPE);

		sounds = GetComponent<AudioSource> ();

		twoPlayers = true;
		player1Ready = false;
		player2Ready = false;
		counting = false;
		keyboardMode = false;

		count = 0;
		player1Result = "";
		player2Result = "";
		player1Score = 0;
		player2Score = 0;
		
		clear = new Vector4(0, 0, 1, 1);
		rock = new Vector4(0, 0, 0, 0);
		scissors = new Vector4(1, 1, 0, 0);
		all = new Vector4(1, 1, 1, 1);

		player1MissText.enabled = false;
		player2MissText.enabled = false;
		player1CountText.enabled = false;
		player2CountText.enabled = false;
		player1ResultText.enabled = false;
		player2ResultText.enabled = false;
		player1KeyboardText.enabled = false;
		player2KeyboardText.enabled = false;
		winnerText.enabled = false;
		verbText.enabled = false;
		loserText.enabled = false;
		instructionsText.enabled = false;
		splashAnimating = false;
		splashScreenPanel.gameObject.SetActive (true);
		startText = splashScreenPanel.FindChild("Start").gameObject.GetComponent<Text>();
		

		if (thumbIndexDistance == 0f) {
			thumbIndexDistance = 35f;
		}
		if (middleRingDistance == 0f) {
			middleRingDistance = 40f;
		}

		/*
		Scissors cut Paper
		Paper covers Rock
		Rock crushes Lizard
		Lizard poisons Spock
		Spock smashes Scissors
		Scissors decapitate Lizard
		Lizard eats Paper
		Paper disproves Spock
		Spock vaporizes Rock
		Rock crushes Scissors


		verbs.Add ("Scissors-Paper", "cut");
		verbs.Add ("Paper-Rock", "covers");
		verbs.Add ("Rock-Lizard", "crushes");
		verbs.Add ("Lizard-Spock", "poisons");
		verbs.Add ("Spock-Scissors", "smashes");
		verbs.Add ("Scissors-Lizard", "decapitate");
		verbs.Add ("Lizard-Paper", "eats");
		verbs.Add ("Paper-Spock", "disproves");
		verbs.Add ("Spock-Rock", "vaporizes");
		verbs.Add ("Rock-Scissors", "crushes");

	}

	void Update ()
	{
		int numHands = 0;
		bool horizSwipe = false;
		bool vertSwipe = false;
		string[] results = new string[2];

		if (!controller.IsConnected) 
		{
			keyboardMode = true;
			player1Ready = true;
			player2Ready = true;
			player1KeyboardText.enabled = true;
			player2KeyboardText.enabled = true;
			player1MissText.enabled = false;
			player2MissText.enabled = false;
		} 
		else 
		{
			keyboardMode = false;
			frame = controller.Frame ();
			numHands = frame.Hands.Count;
			player1KeyboardText.enabled = false;
			player2KeyboardText.enabled = false;
			float position0 = 0f;
			float position1 = 0f;
			player1Ready = false;
			player2Ready = false;
			
			if (numHands > 0)
			{
				position0 = frame.Hands[0].PalmPosition.x;
			}
			if (numHands > 1)
			{
				position1 = frame.Hands[1].PalmPosition.x;
			}
			if (position0 < 0 || position1 < 0)
			{
				player1Ready = true;
				player1MissText.enabled = false;
			}
			if (position0 > 0 || position1 > 0)
			{
				player2Ready = true;
				player2MissText.enabled = false;
			}
		}
		
		// not awaiting Menu Input, so must be awaiting player input
		if (player1Ready && player2Ready)
		{
			/* count is our state engine:
				 * 0: not counting
				 * 1: set to 1 when player swipes to begin play, show 1 on screen
				 * 2: show 2 on screen
				 * 3: show 3 on screen
				 * 4: show "Shoot!" on screen
				 * 5: short delay to let players react to shoot
				 * 6: time to read player gestures and report the winner


			// shooting when count=5 in kb mode or count=6 with leap
			if (count == 6 || (count == 5 && keyboardMode))
			{
				// players are shooting, register gestures
				results = GetPlayerResults ();

				if (player1Result == "") 
				{
					player1Result = results[0];
					player1Model = ShowResultModel(player1Result, true);
				}
				if (player2Result == "")
				{
					player2Result = results[1];
					player2Model = ShowResultModel(player2Result, false);
				}
				// hide each player's count text as they play
				if (player1Result != "")
				{
					player1CountText.enabled = false;
				}
				if (player2Result != "")
				{
					player2CountText.enabled = false;
				}
				if (player1Result != "" && player2Result != "") 
				{
					// have both gestures, so reset count, then show the winner
					count = 0;
					ShowWinner(player1Result, player2Result);
				}
			}
			else if (count > 0) {
				// wait 1 second, then show the count
				if (counting) 
				{
					// subtract deltaTime from the countDown
					countDown -= Time.deltaTime;
					if (countDown <= 0.0f) 
					{
						// 1 second has passed, show and increment the count 
						showCount (count++);
						counting = false;
					}
				}
				else {
					if (count == 5) 
					{
						// reset countDown to short delay to allow Leap players time to react to "shoot!" prompt
						countDown = 0.3f;
					} 
					else 
					{
					// reset countDown to 1 second 
					countDown = 1.0f;
					}
					counting = true;
				}
			}

		}
		else 
		{
			if (count > 0) {
				// players aren't ready - tell them we miss them
				if (!player1Ready) 
				{
					MissPlayer(true);
				}
				if (!player2Ready) 
				{
					MissPlayer(false);
				}
			}
		}

		if (count == 0) 
		{
			// check for continue or back gestures
			if (keyboardMode) 
			{
				startText.text = "Press SPACE to start or ESC to exit";
				instructionsText.text = "Press SPACE to play again or ESC to go back";
			}
			else
			{
				startText.text = "Swipe vertically to start or swipe horizontally to exit";
				instructionsText.text = "Swipe vertically to play again or swipe horizontally to go back";
				for(int g = 0; g < frame.Gestures().Count; g++)
				{
					if (frame.Gestures()[g].Type == Gesture.GestureType.TYPE_SWIPE &&
					    frame.Gestures()[g].State == Gesture.GestureState.STATE_STOP)
					{
						SwipeGesture swipeGesture = new SwipeGesture(frame.Gestures()[g]);
						if (Mathf.Abs(swipeGesture.Direction.x) > Mathf.Abs(swipeGesture.Direction.y))
						{
							horizSwipe = true;
						} 
						else
						{
							vertSwipe = true;
						}
					}
				}
			}
			// if not counting and user hits space bar, play again
			if (Input.GetKey("space") || vertSwipe) 
			{
				if (splashScreenPanel.gameObject.activeSelf)
				{
					splashAnimating = true;
				}
				else 
				{
					ResetBoard(false);
				}
			} 
			// if not counting and user hits escape, go back to the splash screen
			else if (Input.GetKey("escape") || horizSwipe) 
			{
				if (splashScreenPanel.gameObject.activeSelf && !counting) 
				{
					Application.Quit();
					Debug.Log("goodbye!");
				}
				else
				{
					ResetBoard(true);
					// pause for a bit so game doesn't immediately quit
					countDown = 0.5f;
					counting = true;
				}
			}
			if (counting) 
			{
				// subtract deltaTime from the countDown
				countDown -= Time.deltaTime;
				if (countDown <= 0.0f) 
				{
					counting = false;
				}
			}
			if (splashAnimating) {
				splashScreenPanel.Rotate (100.0f * Time.deltaTime, 100.0f * Time.deltaTime, 100.0f * Time.deltaTime);
				splashScreenPanel.Translate(0, 0, -100.0f * Time.deltaTime);
				if (splashScreenPanel.position.z < 10)
				{
					splashScreenPanel.gameObject.SetActive(false);
					splashAnimating = false;
					count = 1;
				}
			}
		}

	}

	// determine whether the hand passed is on the left (player 1) or the right (player 2) of the Leap field
	int GetPlayerOneOrTwo(Hand hand)
	{
		float position = hand.PalmPosition.x;

		if (position < 0) 
		{
			return 0;
		} 
		if (position > 0)
		{
			return 1;
		}
		return 2;
	}

	// get input from db or Leap and assign to player 1 and player 2
	string[] GetPlayerResults() 
	{
		int index = 2;
		string[] results = new string[2];
		results [0] = "";
		results [1] = "";

		if (keyboardMode)
		{
			// read player input from keyboard
			foreach (char c in Input.inputString) 
			{
				switch (c) 
				{
				case '1':
					results[0] = "Rock";
					break;
				case '2':
					results[0] = "Paper";
					break;
				case '3':
					results[0] = "Scissors";
					break;
				case '4': 
					results[0] = "Spock";
					break;
				case '5':
					results[0] = "Lizard";
					break;
				case 'h':
					results[1] = "Rock";
					break;
				case 'j':
					results[1] = "Paper";
					break;
				case 'k':
					results[1] = "Scissors";
					break;
				case 'l': 
					results[1] = "Spock";
					break;
				case ';':
					results[1] = "Lizard";
					break;
				}
			}
		}
		else 
		{
			// read player input from Leap Motion
			if (frame.Hands.Count > 0) 
			{
				foreach (Hand hand in frame.Hands) 
				{
					index = GetPlayerOneOrTwo(hand);
					if (index < 2) 
					{
						results[index] = ReadGesture(hand);
					}
				}
			}
		}
		// if only one player, player 2 results are chosen at random
		if (!twoPlayers) 
		{
			results[1] = GetRandomShoot();
		}

		return results;
	}

	// determine a random result for player 2 to be used in one player games
	string GetRandomShoot() 
	{
		//int index = Random.Range (0, 5);
		// @TODO: pull result from win array
		return "Scissors";
	}

	// Tell the player to put his/her hand into the Leap field
	void MissPlayer(bool isPlayerOne)
	{
		if (isPlayerOne) {
			player1MissText.enabled = true;
		} else {
			player2MissText.enabled = true;
		}
		count = 1;
		player1CountText.enabled = false;
		player2CountText.enabled = false;
	}

	// Examine the position of hand's fingers to determine which gesture the player is making
	string ReadGesture(Hand hand) 
	{
		string shape = "";
		Vector4 handShape = clear;
		Vector[] fingerDirections = new Vector[4];
		Vector firstTipPos;
		Vector secondTipPos;

		for (int i=0; i<4; i++) {
			handShape [i] = hand.Fingers [i + 1].IsExtended ? 1 : 0;
			fingerDirections[i] = hand.Fingers[i].Direction;
		}

		// check distance between thumb 0 and index 1
		firstTipPos = hand.Fingers[0].TipPosition;
		secondTipPos = hand.Fingers[1].TipPosition;
		if (fingerDirections[0].AngleTo(fingerDirections[1]) < 1.3f &&
		    fingerDirections[1].AngleTo(fingerDirections[2]) < 1.3f &&
		    firstTipPos.DistanceTo(secondTipPos) < thumbIndexDistance) {
			shape = "Lizard";
		} else if (handShape == rock) {
			shape = "Rock";
		} else if (handShape == scissors) {
			shape = "Scissors";
		} else if (handShape == all) {
			// check distance between middle 2 and ring 3
			firstTipPos = hand.Fingers[2].TipPosition;
			secondTipPos = hand.Fingers[3].TipPosition;
			float fingerDistance = firstTipPos.DistanceTo(secondTipPos);
			if (fingerDistance > middleRingDistance) {
				shape = "Spock";
			} else {
				shape = "Paper";
			}
		}

		return shape;
	}

	void ResetBoard(bool endOfGame) 
	{
		count = 1;
		player1Result = "";
		player2Result = "";
		Destroy(player1Model);
		Destroy(player2Model);
		winnerText.enabled = false;
		verbText.enabled = false;
		loserText.enabled = false;
		instructionsText.enabled = false;
		if (endOfGame)
		{
			count = 0;
			splashScreenPanel.localPosition = new Vector3(0, 0, 0);
			splashScreenPanel.rotation = Quaternion.identity;
			splashScreenPanel.gameObject.SetActive(true);
			player1Score = 0;
			player2Score = 0;
			player1ScoreText.text = "0";
			player2ScoreText.text = "0";
		}
	}
	
	// Display the count on the screen to prompt the players
	void showCount(int countNum)
	{
		player1CountText.enabled = true;
		player2CountText.enabled = true;

		string countString;
		if (countNum == 4 || countNum == 5) 
		{
			countString = "Shoot!";
			if (countNum == 4) 
			{
				sounds.clip = shootSound;
				sounds.Play();
		
			}
		}
		else 
		{
			countString = countNum.ToString ();
			sounds.clip = countSound;
			sounds.Play();
		}
		player1CountText.text = countString;
		player2CountText.text = countString;

	}

	// display the model corresponding to the result
	Object ShowResultModel(string result, bool isPlayerOne)
	{
		Object model = null;
		int x = 5;
		int qy = 0;
		if (isPlayerOne) 
		{
			x = -5;
		}
		switch (result) 
		{
		case "Rock":
			model = Instantiate(rockModel, new Vector3(x, 1, 0), Quaternion.identity);
			break;
		case "Paper":
			model = Instantiate(paperModel, new Vector3(x, 1, 0), Quaternion.Euler(32, 180, 0));
			break;
		case "Scissors":
			int qx = -125;
			if (isPlayerOne)
			{
				qx = -45;
			}
			model = Instantiate(scissorsModel, new Vector3(x, 1, 0), Quaternion.Euler (qx, 93, 0));
			break;
		case "Spock":
			if (isPlayerOne) 
			{
				qy = 45;
			}
			else 
			{
				qy = 90;
			}
			model = Instantiate(spockModel, new Vector3(x, 1, 0), Quaternion.Euler(-3, qy, -90));
			break;
		case "Lizard":
			qy = 230;
			if (isPlayerOne) 
			{
				qy = 132;
			}
			model = Instantiate(lizardModel, new Vector3(x, 0, 0), Quaternion.Euler(0, qy, 0));
			break;
		}
		return model;
	}

	// Determine winner and look up verb to show how winner won; display winner on screen
	// Ex: Scissors cut Paper
	void ShowWinner(string p1Result, string p2Result) 
	{
		string value = "";

		/*
		// set player result texts and animate results to winner and loser positions
		player1ResultText.text = p1Result;
		player2ResultText.text = p2Result;
		player1ResultText.enabled = true;
		player2ResultText.enabled = true;


		if (verbs.TryGetValue(p1Result + "-" + p2Result, out value)) 
		{
			winnerText.text = p1Result;
			verbText.text = value;
			loserText.text = p2Result;
			player1ScoreText.text = (++player1Score).ToString();
			sounds.clip = winSound;
		} 
		else if (verbs.TryGetValue(p2Result + "-" + p1Result, out value)) 
		{
			winnerText.text = p2Result;
			verbText.text = value;
			loserText.text = p1Result;
			player2ScoreText.text = (++player2Score).ToString();
			sounds.clip = winSound;
		}
		else
		{
			winnerText.text = p1Result;
			verbText.text = "ties";
			loserText.text = p2Result;
			sounds.clip = tieSound;

		}
		
		winnerText.enabled = true;
		verbText.enabled = true;
		loserText.enabled = true;
		instructionsText.enabled = true;
		sounds.Play ();


	}
	*/
}