﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Leap;

/*
 * Smack objects approaching in range: 
 * X: -250 to +250 (-5 to +5): Leap to world scale = 1/50
 * Y: +180 to +400
 * Z: -150 (far) to +300 (near) 
 * 
 * X: -5 to 5
 * Y: 0 to 6
 * Z: 5 to -6
 * 
 * Start:
 * X: -5 to 5
 * Y: 0 to 6
 * Z: 5
 * 
 * Target:
 * X: -2 to 2
 * Y: 0 or 2
 * Z: -6
 */
public class SmackController : MonoBehaviour {

	public int wave = 0;
	public int baddieFactor = 5;
	public float baddieSpeed = 0.5f;
	public int breatherDuration = 3;
	public int timeBetweenSpecials = 1;

	public float startXLeft = -5;
	public float startXRight = 5;
	public float startYTop = 6;
	public float startZ = 4;

	public float finishXLeft = -2;
	public float finishXRight = 2;
	public float finishYTop = 2;
	public float finishZ = -6;

	public GameObject[] zombiePrefabs;
	public GameObject downerFishPrefab;
	public GameObject newIdeaFairyPrefab;
	public GameObject elGlitchPrefab;
	public GameObject flyingBurgerPrefab;
	public GameObject plane;

	public int startingHealth = 20;
	public string waveTitle = "WAVE:";
	public string lastWaveTitle = "Last Wave Completed:";

	public Text healthText;
	public Text waveText;
	public Text lastWaveText;
	public Text countDownText;

	public RectTransform splashScreenPanel;
	public RectTransform deadPanel;

	public AudioClip countSound;
	public AudioClip goSound;
	public AudioClip deadSound;

	[HideInInspector]
	public bool isPaused = true;
	[HideInInspector]
	public int health;

	private enum baddieTypes { zombie, downerFish, newIdeaFairy, elGlitch, flyingBurger };
	private Dictionary<baddieTypes, bool> baddiesActive = new Dictionary<baddieTypes, bool>();
	
	private Controller controller;
	private Frame frame;

	private int numBaddies = 0;
	private bool switchingScreens = false;
	private bool splashAnimating = false;

	private AudioSource sounds = null;

	// Use this for initialization
	void Start () {

		sounds = GetComponent<AudioSource> ();

		plane.gameObject.SetActive (false);
		countDownText.enabled = false;
		splashScreenPanel.gameObject.SetActive (true);

		baddiesActive [baddieTypes.downerFish] = false;
		baddiesActive [baddieTypes.newIdeaFairy] = false;
		baddiesActive [baddieTypes.elGlitch] = false;
		baddiesActive [baddieTypes.flyingBurger] = false;

		// initialize the Leap Motion controller
		controller = new Controller();
		frame = null;

		// enable the circle gesture
		controller.EnableGesture(Gesture.GestureType.TYPECIRCLE);

		health = startingHealth;
		UpdateHealthDisplay ();
		waveText.text = waveTitle + ' ' + wave;
		
	}

	public void DecrementBaddieCount(string baddie) {
		numBaddies--;
		// note if certain baddie types have been deactivated
		switch (baddie) {
		case "Zombie":
			break;
		case "DownerFish":
			baddiesActive[baddieTypes.downerFish] = false;
			break;
		case "NewIdeaFairy":
			baddiesActive[baddieTypes.newIdeaFairy] = false;
			break;
		case "ElGlitch":
			baddiesActive[baddieTypes.elGlitch] = false;
			break;
		case "FlyingBurger":
			baddiesActive[baddieTypes.flyingBurger] = false;
			break;
		}

	}
	
	public void Damage(int damage) {
		health -= damage;
		UpdateHealthDisplay ();
	}

	
	public void SpawnZombieAt(Vector3 spawnPosition) {
		Debug.Log (spawnPosition);
		SpawnBaddie (baddieTypes.zombie, spawnPosition);
	}

	// Update is called once per frame
	void Update () {

		// detect circles for quit or start over, but only while game is paused
		if (isPaused && !switchingScreens && controller.IsConnected) 
		{
			frame = controller.Frame ();
			for(int g = 0; g < frame.Gestures().Count; g++)
			{
				if (frame.Gestures()[g].Type == Gesture.GestureType.TYPE_CIRCLE &&
				    frame.Gestures()[g].State == Gesture.GestureState.STATE_STOP) {

					CircleGesture circle = new CircleGesture(frame.Gestures()[g]);
					// tiny circles are confusing, so only act on bigger ones
					if (circle.Radius > 60f) {
						if (circle.Pointable.Direction.AngleTo(circle.Normal) <= Mathf.PI/2) {
							// clockwise circle
							Debug.Log("cw: " + circle.Radius);
							if (splashScreenPanel.gameObject.activeSelf) {
								// splash screen is active, so animate it out of the way 
								splashAnimating = true;
							} else {
								// splash screen isn't active, so restart game
								ResetGame();
							}
						} else {
							Debug.Log("ccw: " + circle.Radius);
							// counterclockwise circle
							if (splashScreenPanel.gameObject.activeSelf) {
								// on splash screen, so exit
								Application.Quit();
								Debug.Log("goodbye!");
							}
							else {
								StartCoroutine(GoToSplashScreen());
							}
						}
					}
				}
			}
		}

		// Spin the splash screen away at game start
		if (splashAnimating) {
			splashScreenPanel.Rotate (100.0f * Time.deltaTime, 100.0f * Time.deltaTime, 100.0f * Time.deltaTime);
			splashScreenPanel.Translate(0, 0, -100.0f * Time.deltaTime);
			if (splashScreenPanel.position.z < 10)
			{
				// splash screen is out of the way, so disable it
				splashScreenPanel.gameObject.SetActive(false);
				splashAnimating = false;
				// start the game
				ResetGame();
			}
		}
		
		if (health <= 0 && !isPaused) {
			// player is dead
			StartCoroutine(GoToDeadScreen());
		}

		// check whether there are still baddies
		if (!isPaused && numBaddies <= 0) {
			// if no baddies, launch next wave
			StartCoroutine(NextWave());
		}	
	}

	private IEnumerator NextWave() {
		// pause for a few seconds between waves for player to have a isPaused
		isPaused = true;
		switchingScreens = true;
		countDownText.text = breatherDuration.ToString();
		countDownText.enabled = true;
		for (int i = breatherDuration; i>0; i--) {
			// show countdown and beep
			countDownText.text = i.ToString();
			//beep
			sounds.clip = countSound;
			sounds.Play();
			yield return new WaitForSeconds(1);
		}

		// breather has passed, increment wave and spawn new baddies 
		sounds.clip = goSound;
		sounds.Play();
		countDownText.enabled = false;
		isPaused = false;
		switchingScreens = false;
		wave++;
		waveText.text = waveTitle + ' ' + wave;
		SpawnBaddies();
	}

	private IEnumerator GoToSplashScreen() {
		// not on splash screen, so pause game
		isPaused = true;
		// and return to splash screen
		switchingScreens = true;
		plane.gameObject.SetActive(false);
		deadPanel.gameObject.SetActive(false);
		splashScreenPanel.localPosition = new Vector3(0, 0, 0);
		splashScreenPanel.localRotation = Quaternion.identity;
		splashScreenPanel.gameObject.SetActive (true);
		// wait for a moment so player doesn't accidentally exit
		yield return new WaitForSeconds (1);
		switchingScreens = false;

	}

	private IEnumerator GoToDeadScreen() {
		sounds.clip = deadSound;
		sounds.Play();
		// player is dead, pause game 
		isPaused = true;
		// and display dead screen
		switchingScreens = true;
		lastWaveText.text = (wave - 1).ToString();
		waveText.gameObject.SetActive(false);
		plane.gameObject.SetActive(false);
		deadPanel.gameObject.SetActive (true);
		// wait for a moment so player doesn't accidentally exit
		yield return new WaitForSeconds (1);
		switchingScreens = false;
	}

	private void ResetGame() {
		wave = 0;
		numBaddies = 0;
		baddiesActive [baddieTypes.downerFish] = false;
		baddiesActive [baddieTypes.newIdeaFairy] = false;
		baddiesActive [baddieTypes.elGlitch] = false;
		baddiesActive [baddieTypes.flyingBurger] = false;
		health = startingHealth;
		UpdateHealthDisplay ();
		deadPanel.gameObject.SetActive (false);
		waveText.gameObject.SetActive(true);
		plane.gameObject.SetActive(true);
		StartCoroutine (NextWave ());
	}
	
	private void SpawnBaddies() {
		int numZombies = wave * baddieFactor;
		for (int i=0; i < numZombies; i++) {
			SpawnBaddie(baddieTypes.zombie);
		}
		StartCoroutine (SpawnSpecials ());
	}

	private IEnumerator SpawnSpecials() {
		if (!isPaused) {
			// spawn flying burger
			if (health < startingHealth * 0.8 && !baddiesActive[baddieTypes.flyingBurger]) {
				Debug.Log("flyingBurger!");
				SpawnBaddie(baddieTypes.flyingBurger);
			} else {
				int pick = Random.Range(1, 4);
				// spawn downerFish
				if (pick == 1 && !baddiesActive[baddieTypes.downerFish] && wave > 2) {
					Debug.Log("downerFish");
					SpawnBaddie(baddieTypes.downerFish);
				}
				// spawn new idea fairy
				if (pick == 2 && !baddiesActive[baddieTypes.newIdeaFairy] && wave > 3) {
					Debug.Log("newIdeaFairy");
					SpawnBaddie(baddieTypes.newIdeaFairy);
				}
				// spawn el glitch
				if (pick == 3 && !baddiesActive[baddieTypes.elGlitch] && wave > 4) {
					SpawnBaddie(baddieTypes.elGlitch);
					Debug.Log("elGlitch");
				}
			}
			yield return new WaitForSeconds (timeBetweenSpecials);
			StartCoroutine (SpawnSpecials ());
		}

	}

	/*
	 * SpawnBaddie
	 * Input: baddie type
	 * 
	 * Chooses a random starting point and finish point,
	 * then instantiates baddie object and points it toward finish point
	 */
	private void SpawnBaddie(baddieTypes baddie, Vector3 spawnPosition = default(Vector3)) {
		int leftOrRight;
		GameObject prefab = null;
		bool faceFinish = true;

		float startX = Random.Range(startXLeft, startXRight);
		float startY = 0.5f;

		float finishX = Random.Range (finishXLeft, finishXRight);
		float finishY = startY;

		switch (baddie) {
		case baddieTypes.zombie:
			// choose which zombie prefab to use
			if (zombiePrefabs.Length > 0) {
				int zombieIndex = Random.Range(0, zombiePrefabs.Length);
				prefab = zombiePrefabs[zombieIndex];
			}
			break;
		case baddieTypes.downerFish:
			startY = Random.Range(1.0f, startYTop);
			finishY = Random.Range(1.0f, finishYTop);
			prefab = downerFishPrefab;
			baddiesActive[baddieTypes.downerFish] = true;
			break;
		case baddieTypes.newIdeaFairy:
			startY = Random.Range(1.0f, startYTop);
			finishY = Random.Range(1.0f, finishYTop);
			prefab = newIdeaFairyPrefab;
			baddiesActive[baddieTypes.newIdeaFairy] = true;
			break;
		case baddieTypes.elGlitch:
			leftOrRight = Random.Range(-1, 1);
			startX = (leftOrRight < 0) ? startXLeft : startXRight;			
			startZ = Random.Range (startZ/2, finishZ/2);
			prefab = elGlitchPrefab;
			faceFinish = false;
			baddiesActive[baddieTypes.elGlitch] = true;
			break;
		case baddieTypes.flyingBurger:
			leftOrRight = Random.Range(-1, 1);
			startX = (leftOrRight < 0) ? startXLeft : startXRight;			
			startY = Random.Range(1.0f, startYTop/2);
			startZ = Random.Range (startZ/2, finishZ/2);
			prefab = flyingBurgerPrefab;
			faceFinish = false;
			baddiesActive[baddieTypes.flyingBurger] = true;
			break;
		}

		if (prefab != null) {
			// Instantiate baddie
			Vector3 startPosition;
			if (spawnPosition == Vector3.zero) {
				startPosition = new Vector3(startX, startY, startZ);
			} else {
				startPosition = spawnPosition;
			}
			GameObject baddieInstance = null;
			baddieInstance = Instantiate(prefab, startPosition, Quaternion.identity) as GameObject;

			if (baddieInstance != null) {
				// face baddie toward finish point
				if (faceFinish) {
	 				Vector3 finishPosition = new Vector3 (finishX, finishY, finishZ);
					baddieInstance.transform.LookAt(finishPosition);
				}

				// increment baddie count
				numBaddies++;
			}
		}
	}

	private void UpdateHealthDisplay() {
		string healthString = "";
		for (int i=0; i < health; i++) {
			healthString += '+';
		}
		healthText.text = healthString;
	}

}
