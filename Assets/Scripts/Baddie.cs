﻿using UnityEngine;
using System.Collections;

public class Baddie : MonoBehaviour {

	public string handTag;
	public float forceMagnitude = 10.0f;
	public float dieTime = 1.0f;
	public int maxSpeedFactor = 3;
	public int baseDamage = 1;
	public Sprite secondarySprite;
	public AudioClip damageClip = null;
	public AudioClip smackClip = null;

	private BoxCollider box = null;
	private Rigidbody rb = null;
	private AudioSource sounds = null;
	private SpriteRenderer sr = null;
	private GameObject smackController;
	private SmackController smackControllerScript;
	private bool dying = false;
	private float speed;
	private int damage;
	private bool damaging = false;
	private Vector3 direction = Vector3.forward;
	private float lateralFinishX = -10.0f;


	// Use this for initialization
	void Start () {
		handTag =(handTag != "") ? handTag : "Hand";
		box = GetComponent<BoxCollider> ();
		rb = GetComponent<Rigidbody> ();
		sr = GetComponent<SpriteRenderer> ();
		sounds = GetComponent<AudioSource> ();
		smackController = GameObject.FindGameObjectWithTag ("GameController");
		smackControllerScript = smackController.GetComponent<SmackController> ();
		speed = smackControllerScript.baddieSpeed * Random.Range(1,maxSpeedFactor+1);

		// initialize amount of damage, direction, and sprite
		damage = baseDamage;
		switch (gameObject.tag) {
		case "DownerFish":
			damage = baseDamage * 3;
			if (transform.position.x < 0 && secondarySprite != null) {
				sr.sprite = secondarySprite;
			}
			break;
		case "NewIdeaFairy":
			damage = 0;
			if (transform.position.x < 0 && secondarySprite != null) {
				sr.sprite = secondarySprite;
			}
			break;
		case "ElGlitch":
			damage = 0;
			direction = Vector3.right;
			if (transform.position.x > 0) {
				direction *= -1;
				lateralFinishX = smackControllerScript.startXLeft;
			} else {
				lateralFinishX = smackControllerScript.startXRight;
			}
			if (transform.position.x < 0 && secondarySprite != null) {
				sr.sprite = secondarySprite;
			}
			speed = smackControllerScript.baddieSpeed * maxSpeedFactor;
			StartCoroutine (ElGlitchSpawn ());
			break;
		case "FlyingBurger":
			// flying burger does negative damage, ie. heals player
			damage = baseDamage * -2;
			direction = Vector3.right;
			if (transform.position.x > 0) {
				direction *= -1;
				lateralFinishX = smackControllerScript.startXLeft;
			} else {
				lateralFinishX = smackControllerScript.startXRight;
			}
			break;
		}

	}

	void Update() {
		if (smackControllerScript.health <= 0) {
			// player is dead, so I should be, too
			Destroy(gameObject);
		}
	}

	void FixedUpdate () {
		// if I'm not dying (and game isn't paused), move me 
		if (!dying && !smackControllerScript.isPaused) {
			switch (gameObject.tag) {
			case "Zombie":
			case "DownerFish":
				if (transform.position.z > smackControllerScript.finishZ) {
					transform.Translate(direction * Time.fixedDeltaTime * speed);
				} else {
					if (!damaging && !dying) {
						damaging = true;
						StartCoroutine(DamagePlayer());
					}
				}
				break;
			case "NewIdeaFairy":
				if (transform.position.z > smackControllerScript.finishZ) {
					transform.Translate(direction * Time.fixedDeltaTime * speed);
				} else {
					// I've reached the player without being smacked, time to die
					if (!dying) {
						StartCoroutine(Die ());
					}
				}
				break;
			case "ElGlitch":
			case "FlyingBurger":
				if (Mathf.Abs(transform.position.x) <= Mathf.Abs(lateralFinishX)) {
					transform.Translate(direction * Time.fixedDeltaTime * speed);
				} else {
					// I've crossed the plane without being smacked; time to die
					if (!dying) {
						StartCoroutine(Die ());
					}
				}
				break;
			}
		}
	}

	void OnCollisionEnter( Collision collision) {

		// if I've collided with the Hand, smack me
		if (!dying && collision.transform.root.tag == handTag) {
			if (smackClip != null) {
				sounds.clip = smackClip;
				sounds.Play();
			}
			// disable gravity & constraints
			if (rb != null) {
				rb.useGravity = false;
				rb.constraints = RigidbodyConstraints.None;
			}
			// disable box collider so I don't knock over other baddies
			if (box != null) {
				box.enabled = false;
			}
			// Apply the smack force
			rigidbody.AddForce(collision.relativeVelocity * forceMagnitude);

			// Since I've been smacked, start the dying countdown
			StartCoroutine(Die ());

			// handle special cases
			switch (gameObject.tag) {
			case "NewIdeaFairy":
				// New Idea Fairy spawns new zombies on smack
				sounds.clip = damageClip;
				sounds.Play();
				smackControllerScript.SpawnZombieAt(transform.position + (Vector3.right * -1));
				smackControllerScript.SpawnZombieAt(transform.position);
				smackControllerScript.SpawnZombieAt(transform.position + Vector3.right);
				break;
			case "FlyingBurger":
				// Flying Burger does negative damage (heals) on smack
				if (!damaging) {
					damaging = true;
					StartCoroutine(DamagePlayer());
				}
				break;
			}
		}
	}

	// damage player once every second
	private IEnumerator DamagePlayer() {
		sounds.clip = damageClip;
		sounds.Play();
		smackControllerScript.Damage(damage);
		yield return new WaitForSeconds (2);
		StartCoroutine (DamagePlayer ());
	}

	// wait for dieTime, then die
	private IEnumerator Die() {
		// let the rest of my code know I'm dying
		dying = true;
		yield return new WaitForSeconds (dieTime);
		// tell the controller there is one baddie fewer
		smackControllerScript.DecrementBaddieCount(gameObject.tag);
		Destroy (gameObject);
	}

	private IEnumerator ElGlitchSpawn() {
		// wait for a bit, then spawn a zombie!
		yield return new WaitForSeconds(2);
		if (!dying && !smackControllerScript.isPaused) {
			sounds.clip = damageClip;
			sounds.Play();
			smackControllerScript.SpawnZombieAt (transform.position + (Vector3.forward * -1));
			StartCoroutine (ElGlitchSpawn ());
		}
	}

}
